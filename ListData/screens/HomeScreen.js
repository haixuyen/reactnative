import React,{Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { Table, Row, Rows, TableWrapper,Cell } from 'react-native-table-component';
export default class HomeScreen extends Component {
  constructor(props){
    super(props);
    const elementButtonE = (value) => (
      <TouchableOpacity>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Exit</Text>
        </View>
      </TouchableOpacity>
    );
    const elementButtonD = (value) => (
      <TouchableOpacity>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Delete</Text>
        </View>
      </TouchableOpacity>
    );
    this.state={
      tableHead:['ID','TiTle','Category','Location','Status','Exit','Delete'],
      tableData:[
       ['1','BOT Hòa Lạc 14 ngày liên tiếp không thể hoạt động','Thời sự','1','bad',elementButtonE('1'),elementButtonD('1')],
      ['2','Những đòn đánh Trump có thể tung ra với chương trình hạt nhân Iran','Thế giới','1','bad',elementButtonE('2'),elementButtonD('2')],
      ['3','Giá Bitcoin có thể tăng vọt ,vượt 11.000 USD','Kinh doanh','2','good',elementButtonE('3'),elementButtonD('3')],
      ['4','Ái nữ 15 tuổi của Trùm phim xã hội đen Hong Kong  làm mẫu','Giải trí','1','good',elementButtonE('4'),elementButtonD('4')],
      ['5','Pique và cuộc phiêu lưu với đội hạng Năm ở Tây Ban Nha','Thể thao','1','good',elementButtonE('5'),elementButtonD('5')],
      ['6','Người quốc tịch Mỹ lĩnh 12 năm tù tội hoạt động nhằm lật đổ chính quyền','Pháp luật','2','bad',elementButtonE('6'),elementButtonD('6')],
      ['7','Kỹ năng sử dụng Atlat trong bài thi Địa lý Việt Nam','Giáo dục','1','good',elementButtonE('7'),elementButtonD('7')],
      ['8','Răng miện không khỏe có thể làm nguy cơ ung thư gan','Sức khỏe','1','bad',elementButtonE('8'),elementButtonD('8')],
      ['9','6 kiểu tính cách con phản đối mối quan hệ của cha mẹ','Đời sống','1','better',elementButtonE('9'),elementButtonD('9')],
      ['10','Cây tuế duy nhất còn lại trên Trái Đất từ thời khủng long','Khoa học','1','good',elementButtonE('10'),elementButtonD('10')], 
      ['11','Ác mộng của hành khách khi bị bỏ lại một mình trên sân bay','Du lich','0','bad',elementButtonE('11'),elementButtonD('11')],
      ['12','Bill Gates thừa nhận sai lầm lớn nhất của mình ','Số hóa','1','bad',elementButtonE('12'),elementButtonD('12')],
      ['13','Honda Winner thiết kế mới lộ diện tại Việt Nam','Xe','2','bad',elementButtonE('13'),elementButtonD('13')],
      ['14','Nhiều người lấn làn, vượt ẩu vì không xem trọng danh dự bản thân','Ý kiến','2','bad',elementButtonE('14'),elementButtonD('14')],
      ['15','Tôi sợ sẽ tiếp bước truyền thống đổ vỡ hôn nhân của gia đình','Tâm sự','1','bad',elementButtonE('15'),elementButtonD('15')],
      ['16','Cách vợ trừng trị chồng say rượu	','Cười','15','good',elementButtonE('16'),elementButtonD('16')],
      ['17','Câu đố hại não khiến nhiều người chịu thua, còn bạn?','Tiểu phẩm','1','laugh',elementButtonE('17'),elementButtonD('17')],
      ['18','Thành phố không thông minh','Góc nhìn','1','good',elementButtonE('18'),elementButtonD('18')],
      ['19','Hàng trăm nghìn người biểu tình đòi Thủ tướng Czech từ chức','Thế giới','1','bad',elementButtonE('19'),elementButtonD('19')],
      ['20','Cô gái Australia bị loại khỏi Giọng hát Việt ','Giải trí','2','bad',elementButtonE('20'),elementButtonD('20')], 
    ],
    widthArr: [40,200,100,80,100,80,80]
    }
  }
  render(){
    const state=this.state;
  
  return (
    <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  <Rows data={state.tableData} widthArr={state.widthArr} textStyle={styles.dataitem} />
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: 
  { 
    flex: 1,
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#fff' 
  },
  header: 
  { height: 50, 
    backgroundColor: '#537791' ,
  },
  text: 
  { 
    textAlign: 'center', 
    fontWeight: '100' ,
    fontWeight: 'bold'
  },
  dataWrapper: 
  { 
    marginTop: -1 
  },
  row: 
  { 
    height: 40, 
    backgroundColor: '#E7E6E1' 
  },
  dataitem:
  {
    margin:6
  },
  btnText: 
  { 
    textAlign: 'center',
    borderWidth:1,
    borderColor:'black'
  },
  btn: 
  { 
    width: 60, 
    height: 20, 
    marginLeft: 15, 
    backgroundColor: '#c8e1ff', 
    borderRadius: 2 ,
  },
});

HomeScreen.navigationOptions = {
  title: 'Home',
};
