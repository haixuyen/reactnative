import React,{Component} from 'react';
import {View,ScrollView,StyleSheet,da} from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

var items = [
  {
    id: 1,
    name:'Thời sự' ,
  },
  {
    id: 2,
    name:'Thế giới',
  },
  {
    id: 3,
    name:'Kinh doanh',
  },
  {
    id: 4,
    name: 'Giải trí',
  },
  {
    id: 5,
    name: 'Thể thao',
  },
  {
    id: 6,
    name: 'Pháp luật',
  },
  {
    id: 7,
    name: 'Giáo dục',
  },
  {
    id: 8,
    name:'Đời sống',
  },
];
export default class SearchsScreen extends Component {
  constructor(props){
    super(props);
    this.state={
      tableHead:['ID','TiTle','Species','Location','Status'],
      tableData:[
        ['1','BOT Hòa Lạc 14 ngày liên tiếp không thể hoạt động','Thời sự','1','bad'],
       ['2','Những đòn đánh Trump có thể tung ra với chương trình hạt nhân Iran','Thế giới','1','bad'],
       ['3','Giá Bitcoin có thể tăng vọt ,vượt 11.000 USD','Kinh doanh','2','good'],
      ],
      widthArr: [40,200,100,80,100]
    }
  }
  render() {
    const state=this.state;
    return (
      <View>
        < View>
          <SearchableDropdown
            onItemSelect={item => this.setState(item)}
            containerStyle={{ padding: 5 }}
            textInputStyle={{
            padding: 12,
            borderWidth: 1,
            borderColor: '#ccc',
            borderRadius: 5,
          }}
            itemStyle={{
            padding: 10,
            marginTop: 2,
            backgroundColor: '#ddd',
            borderColor: '#bbb',
            borderWidth: 1,
            borderRadius: 5,
          }}
            itemTextStyle={{ color: '#222' }}
            itemsContainerStyle={{ maxHeight: 140 }}
            items={items}
            defaultIndex={2}
            placeholder="placeholder"
            resetValue={false}
            underlineColorAndroid="transparent"
        />
      </View>
      <ScrollView horizontal={true}>
          <View style={{marginTop:50}}>
            <Table borderStyle={{borderWidth:1,marginTop:100,position:'absolute'}}>
              <Row data={state.tableHead} style={styles.head} widthArr={state.widthArr} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  <Rows data={state.tableData} widthArr={state.widthArr} textStyle={styles.dataitem} />
                }
              </Table>
            </ScrollView>
          </View>
          </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: 
  { 
    flex: 1,
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#fff' 
  },
  head: 
  { height: 50, 
    backgroundColor: '#537791' ,
  },
  text: 
  { 

    textAlign: 'center', 
    fontWeight: '100' ,
    fontWeight: 'bold'
  },
  dataitem:
  {
    margin:6
  }
});
SearchsScreen.navigationOptions = {
  title: 'Search',
};