import React,{Component} from 'react';
import { ScrollView, StyleSheet,View,TouchableOpacity } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';

export default class ListsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead:['ID','TiTle','Species','Location','Status'],
      tableData:[
        ['1','BOT Hòa Lạc 14 ngày liên tiếp không thể hoạt động','Thời sự','1','bad'],
        ['2','Những đòn đánh Trump có thể tung ra với chương trình hạt nhân Iran','Thế giới','1','bad'],
        ['3','Giá Bitcoin có thể tăng vọt ,vượt 11.000 USD','Kinh doanh','2','good'],
        ['4','Ái nữ 15 tuổi của Trùm phim xã hội đen Hong Kong  làm mẫu','Giải trí','1','good'],
        ['5','Pique và cuộc phiêu lưu với đội hạng Năm ở Tây Ban Nha','Thể thao','1','good'],
        ['6','Người quốc tịch Mỹ lĩnh 12 năm tù tội hoạt động nhằm lật đổ chính quyền','Pháp luật','2','bad'],
        ['7','Kỹ năng sử dụng Atlat trong bài thi Địa lý Việt Nam','Giáo dục','1','good'],
        ['8','Răng miện không khỏe có thể làm nguy cơ ung thư gan','Sức khỏe','1','bad'],
        ['9','6 kiểu tính cách con phản đối mối quan hệ của cha mẹ','Đời sống','1','better'],
        ['10','Cây tuế duy nhất còn lại trên Trái Đất từ thời khủng long','Khoa học','1','good'], 
        ['11','Ác mộng của hành khách khi bị bỏ lại một mình trên sân bay','Du lich','0','bad'],
        ['12','Bill Gates thừa nhận sai lầm lớn nhất của mình ','Số hóa','1','bad'],
        ['13','Honda Winner thiết kế mới lộ diện tại Việt Nam','Xe','2','bad'],
        ['14','Nhiều người lấn làn, vượt ẩu vì không xem trọng danh dự bản thân','Ý kiến','2','bad'],
        ['15','Tôi sợ sẽ tiếp bước truyền thống đổ vỡ hôn nhân của gia đình','Tâm sự','1','bad'],
        ['16','Cách vợ trừng trị chồng say rượu	','Cười','15','good'],
        ['17','Câu đố hại não khiến nhiều người chịu thua, còn bạn?','Tiểu phẩm','1','laugh'],
        ['18','Thành phố không thông minh','Góc nhìn','1','good'],
        ['19','Hàng trăm nghìn người biểu tình đòi Thủ tướng Czech từ chức','Thế giới','1','bad'],
        ['20','Cô gái Australia bị loại khỏi Giọng hát Việt ','Giải trí','2','bad'], 
      ],
      widthArr: [40,200,100,80,100]
    }
  }
  handlePress = () => {
		const { navigation } = this.props
		navigation.navigate('Detail', {
		title: 'Detail'
		})
	}
 
  render() {
    const state = this.state;
  
    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
          <TouchableOpacity
         style={styles.button}
         onPress={this.handlePress}>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  <Rows data={state.tableData} widthArr={state.widthArr} textStyle={styles.dataitem} />
                }
              </Table>
            </ScrollView>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}
 
const styles = StyleSheet.create({
  container: 
  { 
    flex: 1,
    padding: 16, 
    paddingTop: 30, 
    backgroundColor: '#fff' 
  },
  header: 
  { height: 50, 
    backgroundColor: '#537791' ,
  },
  text: 
  { 
    textAlign: 'center', 
    fontWeight: '100' ,
    fontWeight: 'bold'
  },
  dataWrapper: 
  { 
    marginTop: -1 
  },
  row: 
  { 
    height: 40, 
    backgroundColor: '#E7E6E1' 
  },
  dataitem:
  {
    margin:6
  }
});
ListsScreen.navigationOptions = {
  title: 'List',
};