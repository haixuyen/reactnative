import React,{Component} from 'react';
import { Text,ScrollView, StyleSheet,View,TouchableOpacity,Image } from 'react-native';
export default class DetailScreen extends Component {
    static navigationOptions = {
		title: 'Detail',
	};
    constructor(props){
        super(props);
        this.state={
            head:{
                header:'Chiến lược dồn ép của Iran trước chính sách thiếu nhất quán từ Trump',
                titleMain:'Iran tin rằng Trump ngần ngại sử dụng phương án quân sự nên họ có thể quyết liệt hơn khi đối phó với Mỹ mà không lo lắng về hậu quả.',
                },
            des:{
                desmain:'Tướng Iran cảnh báo sai lầm trả giá bằng mạng sống của Mỹ  /  Cố vấn an ninh Nhà Trắng cảnh báo Iran đừng nghĩ Mỹ yếu đuối',
                img: 'https://i-vnexpress.vnecdn.net/2019/06/25/lead-720-405-3262-1561420775.jpg',
                imgsub:'Lãnh tụ tối cao Iran Ayatollah Ali Khamenei ở Tehran tháng 10/2017. Ảnh: Reuters',
            },
            main:{
                submain1:'Một năm sau, chính quyền Trump chứng kiến các vụ tấn công nhằm vào 4 tàu dầu ở vịnh Ba Tư hồi tháng 5, vụ tấn công hai tàu dầu ở vịnh Oman vào tháng này và Iran bắn hạ máy bay không người lái (UAV) của Mỹ vào tuần trước. Trong một tháng qua, đường ống dẫn dầu cùng với một số sân bay dân sự ở Arab Saudi cũng trở thành mục tiêu tấn công của phiến quân Hồi giáo Houthi được Iran hậu thuẫn ở Yemen. Một số căn cứ quân sự Iraq có binh sĩ và nhà thầu quốc phòng Mỹ đồn trú bị tấn công bằng rocket.',
                submain2:'Trong khi đó, lập trường của giới lãnh đạo Iran khá rõ ràng. Họ từ chối tái thương lượng JCPOA với Mỹ và lãnh đạo tối cao Iran Ayatollah Ali Khamenei khẳng định trong cuộc hội đàm với Thủ tướng Nhật Bản Abe Shinzo hôm 12/6 ở Tehran rằng ông không tin Trump sẵn sàng đàm phán nghiêm túc.',
                submain3:'Iran đã gia tăng dần mức độ quyết liệt khi đối phó với Mỹ. Washington cáo buộc họ tấn công các tàu dầu không chở hàng hồi tháng 5 rồi chuyển sang tấn công các tàu chở đầy ắp nhiên liệu trong tháng 6. Hơn một tuần sau, Iran bắn hạ UAV Mỹ trên eo biển Hormuz',
                submain4:'Sau tuyên bố hủy không kích của Trump, các tướng Iran dường như tự tin hơn trong các phát ngôn của mình, liên tục đe dọa sẽ hủy diệt lực lượng và căn cứ quân sự Mỹ cùng đồng minh ở Trung Đông nếu xung đột nổ ra, thậm chí tuyên bố sẽ tiếp tục bắn hạ UAV Mỹ',
                submain5:'Trên mặt trận ngoại giao, Tehran cũng tìm cách gây sức ép lớn với các nước châu Âu bằng cách đe dọa sau ngày 7/7, nước này sẽ ngừng tuân thủ một số khía cạnh nhất định trong JCPOA, trừ khi các cường quốc châu Âu đưa ra một cơ chế tài chính cho phép Tehran tiếp tục giao dịch với các công ty nước ngoài mà không hứng chịu hậu quả từ biện pháp trừng phạt của Mỹ.',
            },
            end:
            {
                author:'Hồng Vân',
                news:'(Theo ABC Online)'
            }
        }
    }
    render(){
        return(
            <ScrollView>
                <View style={styles.content}>
                    <Text style={styles.header}>{this.state.head.header}</Text>
                    <Text style={styles.titlemain}>{this.state.head.titleMain}</Text>
                    <Text style={styles.desmain}>{this.state.des.desmain}</Text>
                    <Image source={{uri:this.state.des.img}} style={styles.img}/>
                    <Text style={styles.imgsub}>{this.state.des.imgsub}</Text>
                    <Text style={styles.submain}>{this.state.main.submain1}</Text>
                    <Text style={styles.submain}>{this.state.main.submain2}</Text>
                    <Text style={styles.submain}>{this.state.main.submain3}</Text>
                    <Text style={styles.submain}>{this.state.main.submain4}</Text>
                    <Text style={styles.submain}>{this.state.main.submain5}</Text>
                    <View style={styles.item}>
                        <Text style={styles.end}>{this.state.end.author}</Text>
                        <Text>{this.state.end.news}</Text>
                    </View>
                </View>
            </ScrollView>

        );
    }
}
const styles=StyleSheet.create({
    content:
    {
        margin:10
    },
    header:
    {
        fontSize:24,
        fontWeight:'bold',
        marginBottom:10
    },
    titlemain:
    {
        fontSize:16,
        fontWeight:'bold',
        marginBottom:10
    },
    desmain:
    {
        opacity:0.8,
        marginBottom:10
    },
    img:
    {
        width:'100%',
        height:200
    },
    imgsub:
    {
        marginBottom:10,
        backgroundColor:'#f5f5f5'
    },
    submain:
    {
        marginBottom:10
    },
    item:
    {
        flexDirection:'row',
        justifyContent:'flex-end'
    },
    end:
    {
        fontWeight:'bold'
    }
});