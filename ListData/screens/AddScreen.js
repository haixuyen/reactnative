import React,{Component} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import {CheckBox} from 'react-native-elements';
import Select from "react-native-select-plus";
export default class AddScreen extends Component {
  constructor(props){
    super(props);
    this.state={
        ID:'',
        Title:'',
        Species:'',
        Location:'',
        Status:'',
        title:'',
        isTrue:true,
        isFalse:false,
        value: null
    }
    this.items = [
      { key: 1, label: "Thời sự" },
      { key: 2, label: "Thế giới" },
      { key: 3, label: "Kinh doanh" },
      { key: 4, label: "Giải trí" },
      { key: 5, label: "Thể thao" },
      { key: 6, label: "Pháp luật" },
      { key: 7, label:"Giáo dục" },
      { key: 8, label: "Sức khỏe" },
      { key: 9, label: "Đời sống" },
      { key: 10, label: "Khoa học" },
      { key: 11, label: "Du lịch" },
      { key: 12, label: "Số hóa" },
      { key: 13, label: "Tâm sự" },
      { key: 14, label: "Tiểu phẩm" },
      { key: 15, label: "Thế giới" }
    ]; 
  };
    UpdateForm(newState)
      {
        this.setState(newState)
      }
    render(){ 
  return (
      <ScrollView>
        <View style={{margin:5,borderWidth:1,flex:16,borderColor:'red'}}>
          <View style={{flex:2,margin:10}}>
            <Text style={styles.item}>Title:</Text>
            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}} placeholder='Title' value={this.state.Title} onChangeText={(Title) => this.setState({Title})} />
          </View>
          <View style={{flex:2,margin:10}}>
            <Text style={styles.item}>Species:</Text>
            <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}} placeholder='Species' value={this.state.Species} onChangeText={(Species) => this.setState({Species})} />
          </View>
          <View style={{flex:2,margin:10}}>
            <Text style={styles.item}>Location:</Text>
            <CheckBox
              title='Chính trị'
              onPress={()=>this.UpdateForm({isTrue:!this.state.isTrue})}
              checked={this.state.isTrue}/>
            <CheckBox
              title='Quân sự'
              onPress={()=>this.UpdateForm({isFalse:!this.state.isFalse})}
              checked={this.state.isFalse}/>
          </View>
          <View style={{flex:2,margin:10}}>
            <Text style={styles.item}>Status:</Text>
            <Select
              data={this.items}
              width={250}
              placeholder="Select a value ..."
              search={true} />
          </View>
          <View style={{flex:3,margin:10}}>
            <Text style={styles.item}>Mô tả ngắn:</Text>
            <TextInput style={{height: 60, borderColor: 'gray', borderWidth: 1}} placeholder='Mô tả ngắn:' />
          </View>
          <View style={styles.textAreaContainer} >
            <Text style={styles.item}>Mô tả dài:</Text>
            <TextInput
              style={styles.textArea}
              underlineColorAndroid="transparent"
              placeholder="Type something"
              placeholderTextColor="grey"
              numberOfLines={10}
              multiline={true}/>
          </View>
          <View style={{flexDirection:'row',flex:2,margin:10}}>
            <View style={styles.btn}>
              <Button
              title="Add"
              color="white"
              onPress={this.handlePressSave}/>
            </View>
          <View style={styles.btn}>
            < Button
              title="Exit"
              color="white"
              onPress={this.handlePressExit}/>
            </View> 
          </View>
        </View>
      </ScrollView>
  )
  }
}
const styles = StyleSheet.create({
  item:
  {
    marginBottom:5
  },
  textAreaContainer: {
    borderColor: 'black',
    flex:4,
    margin:10
  },
  textArea: {
    height: 150,
    justifyContent: "flex-start",
    borderWidth: 1,
  },
  btn:
  {
    flex:5,
    margin:10,
    borderWidth:1,
    backgroundColor:'green'
  }

})
  AddScreen.navigationOptions = {
    title: 'Add',
  };